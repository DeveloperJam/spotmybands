package controllers


import play.api._
import play.api.mvc._

import play.api.data._
import play.api.data.Forms._

object Application extends Controller {
  
  def index = Action {
    Ok(views.html.index(artistForm))
  }

  def submitArtist = Action{

  	Ok("")
  }

  val artistForm = Form(
   tuple
   (
  "email" -> nonEmptyText,
  "artistID" -> nonEmptyText
  )
)

}