package models

import play.api.db._
import play.api.Play.current
import anorm._
import anorm.SqlParser._


case class UserArtist(email: String, artistID: String)
case class Artist(artistID: String,albumNo: Int)



object UserArtist{
	
	val userArtist = {
		get[String]("email") ~
		get[String]("artistID") map{
			case email~artist => UserArtist(email,artist)
		}
	}

	def followArtist(){

	}
}
object Artist   {

	val artist = {
		get[String]("artistID") ~
		get[Int]("albumNo") map{
			case artist~albumNo => Artist(artist,albumNo)
		}

	}
	def checkAlbumNo(){

	}
	def updateAlbumNo(){

	}
	def checkUpdateEmailAlbumNo(){

	}

	
}
